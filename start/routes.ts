/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

Route.group(() => {
  /* Public Routes */
  Route.post('register', 'AuthController.register')
  Route.post('login', 'AuthController.login')
  Route.get('products/:limit', 'ProductsController.getProducts')
  Route.get('onsale', 'ProductsController.getOnSaleProducts')
  Route.get('products/category/:category', 'ProductsController.getProductsByCategory')
  Route.post('viewbrand', 'ProductsController.getProductsByBrand')
  Route.get('brands', 'BrandsController.getBrands')

  /* Authenticated Routes */
  Route.group(() => {
    /* Authenticated User Routes */
    Route.get('verify/:id', 'AuthController.verifyAuth')
    Route.put('user/update:id', 'AuthController.updateAccount')
    Route.delete('user/delete:id', 'AuthController.deleteAccount')

    /* Authenticated Order routes */
    // Route.get('orders/user/:id', 'OrdersController.getOrdersByUser')
    // Route.post('orders/checkout', 'OrdersController.checkoutOrder')

    /* Authenticated Brand routes */
    Route.post('brand/new', 'BrandsController.createBrand')
    // Route.get('orders/user/:id', 'OrdersController.getOrdersByUser')

    Route.post('comments/:id', 'CommentsController.viewComments')
    Route.post('addcomment', 'CommentsController.addComment')

    Route.post('search', 'ProductsController.searchProduct')

    /* Authenticated Product routes */
    Route.post('products/create', 'ProductsController.createProduct')
    Route.put('products/update/:id', 'ProductsController.updateProduct')
    Route.delete('products/delete/:id', 'ProductsController.deleteProduct')
  }).middleware('auth:api')
}).prefix('api')

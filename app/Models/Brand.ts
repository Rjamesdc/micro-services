import { DateTime } from 'luxon'
import Hash from '@ioc:Adonis/Core/Hash'
import Product from './Product'
import User from './User'
import {
  column,
  beforeSave,
  BaseModel,
  BelongsTo,
  belongsTo,
  hasMany,
  HasMany,
} from '@ioc:Adonis/Lucid/Orm'

export default class Brand extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public userId: number

  @column()
  public productId: number

  @column()
  public brand_name: string

  @column()
  public description: string

  @column()
  public brand_image: string

  @column()
  public is_active?: boolean

  @belongsTo(() => User)
  public users: BelongsTo<typeof User>

  @belongsTo(() => Product)
  public products: BelongsTo<typeof Product>

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}

import { DateTime } from 'luxon'
import Hash from '@ioc:Adonis/Core/Hash'
import Brand from './Brand'
import User from './User'
import {
  column,
  beforeSave,
  BaseModel,
  BelongsTo,
  belongsTo,
  hasMany,
  HasMany,
  hasOne,
  HasOne,
} from '@ioc:Adonis/Lucid/Orm'

export default class Product extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public product_name: string

  @column()
  public userId: number

  @column()
  public brand_id: number

  @column()
  public description: string

  @column()
  public category: string

  @column()
  public heirarchy: string

  @column()
  public product_price: string

  @column()
  public product_price_discount: string

  @column()
  public product_image: string

  @column()
  public positive_rating: number

  @column()
  public negative_rating: number

  @column()
  public product_link: string

  @column()
  public is_active?: boolean

  @column()
  public is_sale?: boolean

  @hasMany(() => Brand, {
    localKey: 'brand_id',
    foreignKey: 'id',
  })
  public brands: HasMany<typeof Brand>

  @belongsTo(() => User)
  public users: BelongsTo<typeof User>

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}

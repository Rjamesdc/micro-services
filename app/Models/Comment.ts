import { DateTime } from 'luxon'
import Hash from '@ioc:Adonis/Core/Hash'
import User from './User'
import Product from './Product'
import {
  column,
  beforeSave,
  BaseModel,
  BelongsTo,
  belongsTo,
  hasMany,
  HasMany,
  hasOne,
  HasOne,
} from '@ioc:Adonis/Lucid/Orm'

export default class Comments extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public userId: number

  @column()
  public productId: number

  @column()
  public comment: string

  @belongsTo(() => User)
  public users: BelongsTo<typeof User>

  @belongsTo(() => Product)
  public products: BelongsTo<typeof Product>

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}

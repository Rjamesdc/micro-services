import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema, rules } from '@ioc:Adonis/Core/Validator'
import axios from 'axios'
import Brand from 'App/Models/Brand'

export default class BrandsController {
  public async getBrands({ response, params }: HttpContextContract) {
    try {
      const brands = await Brand.query().preload('users')
      return response.status(200).json(brands)
    } catch (error) {
      return response.status(404).json({ message: 'Resource not found', error: error })
    }
  }

  public async createBrand({ request, response }: HttpContextContract) {
    const data = request.only([`user_id`, 'brand_name', 'description', 'brand_image'])

    const brandSchema = schema.create({
      brand_name: schema.string({ trim: true }, [
        rules.unique({ table: 'brands', column: 'brand_name' }),
      ]),
    })

    try {
      const payload: any = await request.validate({
        schema: brandSchema,
        messages: {
          'required': 'The {{ field }} is required to create a new account',
          'brand_name.unique': 'Brand already exists',
        },
      })

      if (!payload) return response.status(401).json({ message: payload.message })
      const randomizer: string = Math.floor(Math.random() * 1000).toString()
      const randomImage = await axios.get(`https://picsum.photos/id/${randomizer}/info`)
      const newBrand = new Brand()
      newBrand.userId = data.user_id
      newBrand.brand_image = randomImage.data.url
      newBrand.brand_name = data.brand_name
      newBrand.description = data.description
      const brandData = await newBrand.save()
      return response.status(200).json({
        brandData,
        message: 'Brand Addded Successfully',
      })
    } catch (error) {
      response.badRequest(error.messages)
    }
  }

  public async updateBrand({ params, request, response }: HttpContextContract) {
    const data = request.only(['brand_name', 'description', 'is_active'])
    const existingBrand = await Brand.find(params.id)
    if (!existingBrand) {
      return response.status(404).json({ message: 'Resource not found' })
    }

    const brandSchema = schema.create({
      brand_name: schema.string({ trim: true }, [
        rules.unique({ table: 'brands', column: 'brand_name' }),
      ]),
    })

    const validatedData = await request.validate({
      schema: brandSchema,
    })

    if (!validatedData) {
      return response.status(422).json({ message: 'Something went wrong. please try again.' })
    }

    existingBrand.brand_name = data.brand_name
    existingBrand.description = data.description
    existingBrand.is_active = data.is_active

    await existingBrand.save() // 👈 Update / persists changes in database
    return response.status(200).json({ message: 'Brand successfully updated.', existingBrand })
  }

  public async deleteBrand({ params, response }: HttpContextContract) {
    const brand = await Brand.query().where('id', params.id).delete()
    if (brand) {
      return response.status(200).json({ message: 'Brand successfully deleted' })
    }
  }
}

import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Product from 'App/Models/Product'
import { schema, rules } from '@ioc:Adonis/Core/Validator'
import axios from 'axios'

export default class ProductsController {
  public async getProducts({ response, params }: HttpContextContract) {
    try {
      const products = await Product.query()
        .preload('users')
        .preload('brands')
        .where('is_sale', 0)
        .andWhere('is_active', 1)
        .limit(params.limit)

      return response.status(200).json(products)
    } catch (error) {
      console.log(error)
      return response.status(404).json({ message: 'Resource not found', error: error })
    }
  }

  public async getOnSaleProducts({ response, params }: HttpContextContract) {
    try {
      const onSales = await Product.query()
        .preload('users')
        .preload('brands')
        .where('is_acitve', 1)
        .andWhere('is_sale', 1)

      return response.status(200).json(onSales)
    } catch (error) {
      return response
        .status(404)
        .json({ message: 'Something went wrong, please try again.', error: error })
    }
  }

  public async searchProduct({ request, response, params }: HttpContextContract) {
    const data = request.only(['search'])
    try {
      const products = await Product.query()
        .preload('users')
        .preload('brands')
        .where('product_name', 'LIKE', '%' + data.search + '%')

      if (products) return response.status(200).json({ message: 'Success', products })
    } catch (error) {
      return response.status(400).json({ message: 'Resource not found' })
    }
  }

  public async getProductsByCategory({ response, params }: HttpContextContract) {
    try {
      const product = await Product.query().where('heirarchy', params.category).preload('users')
      if (!product) {
        return response.status(404).json({ message: 'Resource not found' })
      }
      return response.status(200).json({ message: 'Success', product })
    } catch (e: any) {
      response.status(404).json({ message: 'Something went wrong, please try again' })
    }
  }

  public async getProductsByBrand({ response, request }: HttpContextContract) {
    const data = request.only(['brand_id'])
    try {
      const product = await Product.query().where('brand_id', data.brand_id).preload('brands')
      if (!product) {
        return response.status(404).json({ message: 'Resource not found' })
      }
      return response.status(200).json({ message: 'Success', product })
    } catch (e: any) {
      response.status(404).json({ message: 'Something went wrong, please try again' })
    }
  }

  public async createProduct({ request, response }: HttpContextContract) {
    const data = request.only([
      `user_id`,
      `brand_id`,
      'product_name',
      'description',
      'product_image',
      'hierarchy',
      'category',
      'product_price_discount',
      'product_price',
      'product_link',
    ])

    const productSchema = schema.create({
      product_name: schema.string({ trim: true }, [
        rules.unique({ table: 'products', column: 'product_name' }),
      ]),
    })

    try {
      const payload: any = await request.validate({
        schema: productSchema,
        messages: {
          'required': 'The {{ field }} is required to create a new account',
          'produc_name.unique': 'Product already exists',
        },
      })

      if (!payload) return response.status(401).json({ message: payload.message })
      const randomizer: string = Math.floor(Math.random() * 1000).toString()
      const randomImage = await axios.get(`https://picsum.photos/id/${randomizer}/info`)
      const newProduct = new Product()
      newProduct.userId = data.user_id
      newProduct.brand_id = data.brand_id
      newProduct.product_image = randomImage.data.url
      newProduct.product_name = data.product_name
      newProduct.category = data.category
      newProduct.description = data.description
      newProduct.product_price = data.product_price
      newProduct.product_price_discount = data.product_price_discount
      newProduct.heirarchy = data.hierarchy
      newProduct.product_link = data.product_link
      const productData = await newProduct.save()
      return response.status(200).json({
        productData,
        message: 'Product Addded Successfully',
      })
    } catch (error) {
      response.badRequest(error.messages)
    }
  }

  public async updateProduct({ params, request, response }: HttpContextContract) {
    const data = request.only([
      'product_name',
      'description',
      'product_price',
      'product_image',
      'product_price_discount',
      'hierarchy',
      'product_link',
      'is_sale',
      'is_active',
    ])
    const existingProduct = await Product.find(params.id)
    if (!existingProduct) {
      return response.status(404).json({ message: 'Resource not found' })
    }

    const productSchema = schema.create({
      product_name: schema.string({ trim: true }, [
        rules.unique({ table: 'products', column: 'product_name' }),
      ]),
    })

    const validatedData = await request.validate({
      schema: productSchema,
    })

    if (validatedData) {
      existingProduct.product_name = data.product_name
      existingProduct.description = data.description
      existingProduct.product_price = data.product_price
      existingProduct.product_price_discount = data.product_price_discount
      existingProduct.heirarchy = data.hierarchy
      existingProduct.product_link = data.product_link
      existingProduct.is_sale = data.is_sale
      existingProduct.is_active = data.is_active

      await existingProduct.save() // 👈 Update / persist changes in Database.
      return response.status(201).json({ message: 'Product updated successfully', existingProduct })
    }
  }

  public async deleteProduct({ params, response }: HttpContextContract) {
    const product = await Product.query().where('id', params.id).delete()
    if (product) {
      return response.status(200).json({ message: 'Product successfully deleted' })
    }
  }
}

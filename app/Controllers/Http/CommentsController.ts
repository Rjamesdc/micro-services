import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Comments from 'App/Models/Comment'

export default class CommentsController {
  public async addComment({ request, response }: HttpContextContract) {
    const data = request.only([`user_id`, 'comment', 'product_id'])

    try {
      const newComment = new Comments()
      newComment.userId = data.user_id
      newComment.comment = data.comment
      newComment.productId = data.product_id
      const commentData = await newComment.save()
      return response.status(200).json({
        commentData,
        message: 'Comment Addded Successfully',
      })
    } catch (error) {
      response.badRequest(error.messages)
    }
  }

  public async viewComments({ params, request, response }: HttpContextContract) {
    const commentData = await Comments.query()
      .where('product_id', params.id)
      .preload('users')
      .preload('products')
    if (!commentData) return response.status(404).json({ message: 'Resource not found' })
    return response.status(200).json({ commentData })
  }
}

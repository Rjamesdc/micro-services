'use strict'

import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User'
import { schema, rules } from '@ioc:Adonis/Core/Validator'
import axios from 'axios'

export default class AuthController {
  public async login({ request, auth, response }: HttpContextContract) {
    const data = request.only(['email', 'password'])
    const userExists = await User.findBy('email', data.email) // 👈 ORM operation
    if (!userExists) {
      return response.status(401).json({ message: 'Invalid user credentials' })
    } // 🔎 Check if User Exists in DB

    try {
      const token = await auth.use('api').attempt(data.email, data.password, {
        expiresIn: '365 days',
      })

      return response
        .status(200)
        .json({ message: 'Successfully logged in', session: userExists, token: token.toJSON() })
    } catch (e) {
      return response.status(401).json({ message: 'Invalid credentials provided' })
    }
  }

  public async register({ request, auth, response }: HttpContextContract) {
    const data = request.only(['firstname', 'lastname', 'email', 'password', 'user_name'])
    const userSchema = schema.create({
      email: schema.string({ trim: true }, [rules.unique({ table: 'users', column: 'email' })]),
      password: schema.string({ trim: true }),
    })

    try {
      const payload: any = await request.validate({
        schema: userSchema,
        messages: {
          'required': '{{ field }} is required to create a new account',
          'email.unique': 'Email address already in use.',
        },
      })

      if (!payload) return response.status(401).json({ message: payload.message })
      const randomizer: string = Math.floor(Math.random() * 1000).toString()
      const randomImage = await axios.get(`https://picsum.photos/id/${randomizer}/info`)
      const newUser = new User()
      newUser.user_name = data.user_name
      newUser.firstname = data.firstname
      newUser.lastname = data.lastname
      newUser.user_image = randomImage.data.url
      newUser.email = data.email
      newUser.password = data.password
      await newUser.save()
      const token = await auth.use('api').login(newUser, {
        expiresIn: '365 days',
      })
      return token.toJSON()
    } catch (error) {
      return response.status(422).json({ error })
    }
  }

  public async updateAccount({ params, request, response }: HttpContextContract) {
    const data = request.only(['firstname', 'lastname', 'email', 'password', 'user_name'])
    const userData = await User.find('id', params.id)
    if (!userData) {
      return response.status(404).json({ message: 'Resource not found' })
    }

    const updatedUser = new User()
    updatedUser.email = data.email
    updatedUser.firstname = data.firstname
    updatedUser.lastname = data.lastname
    updatedUser.password = data.password
    updatedUser.user_name = data.user_name

    await updatedUser.save()
    return response.status(201).json({ message: 'Account updated successfully', user: updatedUser })
  }

  public async verifyAuth({ params, response }: HttpContextContract) {
    try {
      const user: any = await User.find(params.id)
      if (!user) {
        return response.status(404).json({ message: 'Resource not found' })
      }
      return response.status(200).json({ message: 'Authorized' })
    } catch (e: any) {
      return response.status(401).json({ message: 'E_UNAUTHORIZED_ACCESS: Unauthorized access' })
    }
  }

  public async deleteAccount({ params, response }: HttpContextContract) {
    try {
      const user = await User.query().where('id', params.id).delete()
      if (!user) return response.status(400).json({ message: 'Resource not found' })
      return response.status(200).json({ message: 'User account successfully deleted' })
    } catch (err: any) {
      return response
        .status(422)
        .json({ message: 'Something went wrong, cant proceed with operation' })
    }
  }
}

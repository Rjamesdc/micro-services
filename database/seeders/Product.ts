import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Product from 'App/Models/Product'

// newProduct.userId = data.user_id
// newProduct.brand_id = data.brand_id
// newProduct.product_image = randomImage.data.url
// newProduct.product_name = data.product_name
// newProduct.category = data.category
// newProduct.description = data.description
// newProduct.product_price = data.product_price
// newProduct.product_price_discount = data.product_price_discount
// newProduct.heirarchy = data.hierarchy
// newProduct.product_link = data.product_link

export default class ProductSeeder extends BaseSeeder {
  public async run() {
    let i = 0
    let count = 10

    while (i < count) {
      await Product.create({
        userId: 1,
        brand_id: 1,
        product_image:
          'https://www.pulsecarshalton.co.uk/wp-content/uploads/2016/08/jk-placeholder-image.jpg',
        product_name: `Test Product${i}`,
        category: 'Shirts',
        description: `Sample Products${i}`,
        product_price: `${i + 100}`,
        product_price_discount: '0',
        heirarchy: 'Top 50',
        product_link: 'https://www.littlebirdie.com.au/shop/deal/reverse-weave-hoodie-397043200',
      })

      i++
    }
  }
}

import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import User from 'App/Models/User'

// newUser.user_name = data.user_name
// newUser.firstname = data.firstname
// newUser.lastname = data.lastname
// newUser.user_image = randomImage.data.url
// newUser.email = data.email
// newUser.password = data.password

export default class UserSeeder extends BaseSeeder {
  public async run() {
    let i = 0
    let count = 10

    while (i < count) {
      const randomF = Math.random().toString(36).substr(2, 5)
      const randomL = Math.random().toString(36).substr(2, 7)

      await User.create({
        user_name: `OppoNoppo${i}`,
        firstname: `${randomF}`,
        lastname: `${randomL}`,
        user_image:
          'https://image.shutterstock.com/image-illustration/male-default-placeholder-avatar-profile-260nw-582509551.jpg',
        email: `${randomF}${randomL}${i}@gmail.com`,
        password: '12345',
      })

      i++
    }
  }
}

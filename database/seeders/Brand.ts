import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Brand from 'App/Models/Brand'

// newBrand.userId = data.user_id
// newBrand.brand_image = randomImage.data.url
// newBrand.brand_name = data.brand_name
// newBrand.description = data.description

export default class BrandSeeder extends BaseSeeder {
  public async run() {
    let i = 0
    let count = 10

    while (i < count) {
      const r = (Math.random() + 1).toString(36).substring(7)
      await Brand.create({
        userId: 1,
        brand_image: 'https://designshack.net/wp-content/uploads/placeholder-image.png',
        brand_name: `productTest${r}${i}`,
        description: 'Product Brand Test',
      })

      i++
    }
  }
}

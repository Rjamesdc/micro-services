import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Products extends BaseSchema {
  protected tableName = 'products'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('user_id').unsigned().references('users.id').onDelete('CASCADE') // delete post when user is deleted
      table.integer('brand_id').unsigned().references('brands.id')
      table.string('product_name', 255).notNullable()
      table.string('category', 255).notNullable()
      table.string('description', 255).notNullable()
      table.string('heirarchy', 25).notNullable()
      table.string('product_price', 100).notNullable()
      table.string('product_price_discount', 100).nullable().defaultTo(0)
      table.string('product_image', 255).notNullable()
      table.string('product_link', 255).notNullable()
      table.integer('positive_rating', 255).nullable().defaultTo(0)
      table.integer('negtaive_rating', 255).nullable().defaultTo(0)
      table.boolean('is_active').notNullable().defaultTo(true)
      table.boolean('is_sale').notNullable().defaultTo(false)
      table.timestamps(true)
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
